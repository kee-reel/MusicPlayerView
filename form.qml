import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import Qt.labs.platform 1.1

Item {
	id: root
	property var ratio: 1
	property var musicPlayerObj: musicPlayer.object
	property var deleteMode: false
	property var deletedTrackIndices: []
	signal deleteSelectedTracks()
	
	Rectangle {
		anchors.fill: parent
		color: "#2e3440"
	}
	
	ColumnLayout {
		anchors.fill: parent
    ScrollView {
        id: scrollView
        contentWidth: width
        clip: true
       	Layout.preferredHeight: parent.height * 0.8
       	Layout.preferredWidth: parent.width
        ScrollBar.horizontal.policy: ScrollBar.AlwaysOff
        ScrollBar.vertical.policy: ScrollBar.AlwaysOff

        ListView {
            id: listView
            anchors.fill: parent
            model: tracks
            boundsBehavior: Flickable.StopAtBounds
			delegate: Button {
				property var needToDelete: needToDeleteCheckBox.checked
                width: parent.width
                height: 50
                text: model.path
                background: Rectangle {
                	color: needToDelete ? "red" : musicPlayerObj.trackIndex === index ? "gray" : "white"
                }
                onClicked: {
                	musicPlayerObj.trackIndex = index
                }
                CheckBox {
                	id: needToDeleteCheckBox
               		visible: deleteMode
                	anchors.right: parent.right
                	anchors.verticalCenter: parent.verticalCenter
                	Component.onCompleted: {
                		deleteSelectedTracks.connect(onDeleteTracks)
                	}
                	function onDeleteTracks()
                	{
                		if(needToDelete) {
							musicPlayerObj.removeTrack(index)
						}
                	}
                }
			}
        }
    }
		RowLayout {
			Layout.alignment: Qt.AlignHCenter | Qt.AlignBottom
			Button {
				Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
				id: prevButton
				text: "Prev"
				font.pixelSize: 18 * ratio
				onClicked: musicPlayerObj.prev()
				background: Rectangle {
					color: parent.pressed ? "#d8dee9" : "#81a1c1"
					radius: 90
				}
			}
			Button {
				Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
				id: playButton
				text: "Play"
				font.pixelSize: 18 * ratio
				onClicked: musicPlayerObj.play()
				background: Rectangle {
					color: parent.pressed ? "#d8dee9" : "#81a1c1"
					radius: 90
				}
			}
			Button {
				Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
				id: nextButton
				text: "Next"
				font.pixelSize: 18 * ratio
				onClicked: musicPlayerObj.next()
				background: Rectangle {
					color: parent.pressed ? "#d8dee9" : "#81a1c1"
					radius: 90
				}
			}
			Button {
				Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
				id: addButton
				text: "Add"
				font.pixelSize: 18 * ratio
				onClicked: fileDialog.open()
				background: Rectangle {
					color: parent.pressed ? "#d8dee9" : "#81a1c1"
					radius: 90
				}
			}
			Button {
				Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
				id: removeButton
				text: "Remove"
				font.pixelSize: 18 * ratio
				onClicked: {
					deleteMode = !deleteMode
					if(!deleteMode) {
						deleteSelectedTracks()
					}
				}
				background: Rectangle {
					color: parent.pressed ? "#d8dee9" : "#81a1c1"
					radius: 90
				}
			}
			FileDialog {
			    id: fileDialog
			    title: "Please choose a file"
			    folder: shortcuts.home
			    fileMode: FileDialog.OpenFiles
			    onAccepted: {
				 musicPlayerObj.addTracks(fileDialog.fileUrls)
			    }
			}
		}
	}
	
	Button {
		id: exitButton
		text: "Back"
		font.pixelSize: 18 * ratio
		anchors.left: parent.left
		anchors.top: parent.top
		anchors.margins: 10 * ratio
		width: 60 * ratio
		height: width
		onClicked: uiElement.closeSelf()
		background: Rectangle {
			color: exitButton.pressed ? "#d8dee9" : "#81a1c1"
			radius: 90
		}
	}
}

#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"

#include "../../Interfaces/Architecture/GUIElementBase/guielementbase.h"

#include "../../Interfaces/Utility/imusicplayer.h"
#include "../../Interfaces/Utility/i_music_player_track_data_extention.h"

class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.MusicPlayerView" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	~Plugin() override;

private:
	GUIElementBase* m_GUIElementBase;
	ReferenceInstancePtr<IMusicPlayer> m_musicPlayer;
	ReferenceInstancePtr<IMusicPlayerTrackDataExtention> m_tracks;
	QPointer<IExtendableDataModelFilter> m_filter;

	// PluginBase interface
protected:
	void onReady() override;
};
